# Perle

A programming language for music composition.

The goal is to allow composers to abstract their music instead of writing in "musical assembly".

For example, melody of the first few bars of Beethoven's Fifth might be written as this

```
-- Transposes a phrase down
transp : Interval -> Phrase -> Phrase

motifA = [(_ g) (g g) | (e)]

intro = [Eb, 2/4: motifA | (seqD (Up 1) motifA)]
```

Ideally the language would be flexible enough to accommodate non-tonal or micro-tonal systems, but that's WIP.

Expressions within the language would be "compiled" into different targets like midi or exported as sheet music.

## Basics

The language is very flexible and allows for aspects of the music to be left ambiguous and resolved later on. For instance, pitch classes can be specified and the actual octaves figured out later.

### Pitches

Notes by letter

```
[c d e f]
```

Notes can be tied via replacing the repeated note value with `=`

```
[g = = eb]
```

Rests are via `_`

```
[_ g = =]
```

Vertical line starts a new bar

```
[a b c d | b c d e]
```

Number after a pitch specifies octave

```
[c4 f4 c5 b4]
```

One can specify the note above or below the previous
- `'` for above
- `,` for below

```
[c f' c' b,]
```

If a note is within a major 3rd of the previous, direction will be changed in that direction

```
[c f' c' b, | c e d e]
```

Notes farther away than a major 3rd require annotation

```
[c f' c, f']
```

Trills are created with a ~

```
[c~b d e f]
```

Sharps and flats are added like so

```
[c# eb f## gbb]
```

### Keys

The Key can be specified at the beginning of a bar

```
[A: a b c d | e f g a]
```

is the same as

```
[a b c# d | e f# g# a]
```

for minor keys use the lowercase note name

```
[d: d e f g | b c e d]
```

is the same as

```
[d e f g | bb c e d]
```

### Rhythm

Two bars of 4/4

```
[4/4: c d e f | c d e f]
```

Key and Meter specified

```
[Eb, 2/4: (_ g g g) | (eb) ]
```

If time sig is specified, then following bars are checked for consistence

This would be an error

```
[4/4: c d e]
```

If one wants a tuple they need to wrap in parenthesis this would be 3 half note tuples

```
[4/4: (c d e)]
```

Half notes in 4/4 are thus

```
[4/4: (c) (d)]
```

Parenthesized notes take up the remaining time in the bar, so this would be 1 + 3

```
[4/4: c (d)]
```

dotted rhythms like so

```
[4/4: c. c. c]
```

note that parenthesis do the actual division of the bar, so this would be groups of three 8th notes

```
[9/8: (c) (c) (d = e)]
```

While this would be doted 8ths

```
[9/8: c. c. c. c. c. c.]
```

The meter can be made flexible by leaving the top out. This would be 3/4 followed by 4/4

```
[4: c d e | a b c d]
```

In a flexible meter, parenthesis always divide the note value

```
[4: (c d e f) | (a b) (c d)]
```

is the same as

```
[1/4: (c d e f) | 2/4: (a b) (c d)]
```
