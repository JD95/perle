module Perle.Group where

class Monoid a => Group a where
  inv :: a -> a
