{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DerivingStrategies #-}

module Perle.Logic where

import Data.Maybe
import Control.Monad
import Data.List
import Control.Applicative
import qualified Data.List as List
import Data.Set (Set)
import qualified Data.Set as Set
import GHC.Word

import Perle.Runtime.Prop
import Perle.Runtime.Ref
import Perle.Mod
import Perle.Lattice

import Perle.Lattice.Diet (Diet)
import qualified Perle.Lattice.Diet as Diet
import GHC.RTS.Flags (DebugFlags)

type Val m a = Cell m (Ref m) (Maybe a)

newtype Fuzzy a = Fuzzy (Set a)
  deriving (Eq)

newtype PitchSet = PitchSet { unPitchSet :: Diet Word8 }
  deriving Show

instance Lattice PitchSet where
  bottom = PitchSet $ Diet.singleton 0 127

  isTop (PitchSet d) = Diet.isPoint d

  merge (Old (PitchSet x)) (New (PitchSet y)) = PitchSet <$> merge (Old x) (New y)

ofPitchClass :: [Word8] -> [Word8]
ofPitchClass notes =
  filter (\x -> (x `mod` 12) `elem` notes) [0..127]

groupInterval :: [Word8] -> [(Word8, Word8)]
groupInterval =
  fmap (\xs -> (head xs, last xs))
  . groupBy (\x y -> y - x <= 2)

pitchClasses :: [Word8] -> [(Word8, Word8)]
pitchClasses = groupInterval . ofPitchClass

printPitch :: Word8 -> String
printPitch n = pitches !! fromIntegral n where
  pitches =
    [ "C" , "Db" , "D" , "Eb"
    , "E" , "F" , "Gb" , "G"
    , "Ab" , "A" , "Bb" , "B"
    ]

pitchIntervals :: [Word8] -> PitchSet
pitchIntervals = PitchSet . Diet.fromIntervals . pitchClasses

pitchIsolates :: [Word8] -> PitchSet
pitchIsolates =
  PitchSet . Diet.fromIntervals . fmap (\x -> (x,x)) . ofPitchClass

data PitchClass
  = C
  | D
  | E
  | F
  | G
  | A
  | B
  deriving (Show, Eq, Ord)

data Accidental
  = Sharp
  | Flat
  | Nat
  deriving (Show, Eq, Ord)

data Pitch = Pitch (Maybe PitchClass) (Maybe Accidental)
  deriving (Show)

instance Lattice Pitch where
  bottom = Pitch Nothing Nothing
  isTop (Pitch (Just _) (Just _)) = True
  isTop _ = False
  merge (Old (Pitch oldX oldY)) (New (Pitch newX newY)) =
    uncurry Pitch <$> merge (Old (oldX, oldY)) (New (newX, newY))

pitch :: (Alternative m, MonadRef m) => Cell m (Ref m) Pitch -> Cell m (Ref m) PitchSet -> m ()
pitch pRef sRef = push [Watched pRef] sRef $ learnPitch <$> value pRef

learnPitch :: Pitch -> PitchSet
learnPitch (Pitch Nothing Nothing) = bottom
learnPitch (Pitch (Just p) Nothing) = learnClass p
learnPitch (Pitch Nothing (Just acc)) = learnAccidentals acc
learnPitch (Pitch (Just p) (Just acc)) =
  case (p, acc) of
    --
    (C, Nat) -> pitchIntervals [0]
    (C, Sharp) -> pitchIntervals [1]
    (C, Flat) -> pitchIntervals [11]
    --
    (D, Nat) -> pitchIntervals [2]
    (D, Sharp) -> pitchIntervals [3]
    (D, Flat) -> pitchIntervals [1]
    --
    (E, Nat) -> pitchIntervals [4]
    (E, Sharp) -> pitchIntervals [5]
    (E, Flat) -> pitchIntervals [3]
    --
    (F, Nat) -> pitchIntervals [5]
    (F, Sharp) -> pitchIntervals [6]
    (F, Flat) -> pitchIntervals [4]
    --
    (G, Nat) -> pitchIntervals [7]
    (G, Sharp) -> pitchIntervals [8]
    (G, Flat) -> pitchIntervals [6]
    --
    (A, Nat) -> pitchIntervals [9]
    (A, Sharp) -> pitchIntervals [10]
    (A, Flat) -> pitchIntervals [8]
    --
    (B, Nat) -> pitchIntervals [11]
    (B, Sharp) -> pitchIntervals [0]
    (B, Flat) -> pitchIntervals [10]

learnClass :: PitchClass -> PitchSet
learnClass C = pitchIntervals [0,1,11]
learnClass D = pitchIntervals [1,2,3]
learnClass E = pitchIntervals [3,4,5]
learnClass F = pitchIntervals [4,5,6]
learnClass G = pitchIntervals [6,7,8]
learnClass A = pitchIntervals [8,9,10]
learnClass B = pitchIntervals [10,11,1]

learnAccidentals :: Accidental -> PitchSet
learnAccidentals Nat = pitchIsolates [0,2,4,5,7,9,11]
learnAccidentals Sharp = pitchIsolates [0,1,3,5,6,8,10]
learnAccidentals Flat = pitchIsolates [1,3,4,6,8,10,11]

key :: PitchClass -> Accidental -> (PitchClass -> Accidental)
key C Nat _ = Nat
key _ _ _ = undefined

range ::
  Maybe Word8 ->
  Maybe Word8 ->
  PitchSet
range start end = PitchSet $ Diet.singleton (fromMaybe 0 start) (fromMaybe 127 end)

inKey ::
  (MonadRef m, Alternative m) =>
  PitchClass ->
  Accidental ->
  Cell m (Ref m) Pitch ->
  m (Cell m (Ref m) Pitch)
inKey c a p = do
  result <- cell bottom
  push [Watched p] result $ do
    Pitch p _ <- value p
    pure $ Pitch p (key c a <$> p)
  pure result

-- * Octave Primitives

-- relativeOctave ::
--   (Alternative m, MonadRef m) =>
--   (PitchClass -> Octave -> PitchClass -> Octave) ->
--   Val m PitchClass ->
--   Val m Octave ->
--   Val m PitchClass ->
--   m (Val m Expr)
-- relativeOctave f prevPitch prevOct pitch = do
--   result <- cell Nothing
--   prop [Watched prevOct, Watched pitch] result $ do
--     pOld <- readRef (value prevPitch)
--     oct <- readRef (value prevOct)
--     pNew <- readRef (value pitch)
--     pure $ Pitch <$> pNew <*> (f <$> pOld <*> oct <*> pNew)
--   pure result
--
-- octAbove ::
--   (Alternative m, MonadRef m) =>
--   Val m PitchClass ->
--   Val m Octave ->
--   Val m PitchClass ->
--   m (Val m Expr)
-- octAbove = relativeOctave $ \pOld oct pNew ->
--   if pNew < pOld
--     then upOctave oct 1
--     else oct
--
-- octBelow ::
--   (Alternative m, MonadRef m) =>
--   Val m PitchClass ->
--   Val m Octave ->
--   Val m PitchClass ->
--   m (Val m Expr)
-- octBelow = relativeOctave $ \pOld oct pNew ->
--   if pOld < pNew
--     then downOctave oct 1
--     else oct
--
-- -- * Interval Primitives
-- newtype Interval = Interval Int
--
-- -- * Pitch Primitives
--
-- -- * Cannonic Primitives
-- transposition :: Interval -> Val m Expr -> m (Val m Expr)
-- transposition = undefined
--
-- inversion :: Val m Expr -> m (Val m Expr)
-- inversion = undefined
--
-- retrograde :: Val m Expr -> m (Val m Expr)
-- retrograde = undefined
--
-- diminution :: Rational -> Val m Expr -> m (Val m Expr)
-- diminution = undefined
--
-- augmentation :: Rational -> Val m Expr -> m (Val m Expr)
-- augmentation = undefined
