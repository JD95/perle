{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DerivingStrategies #-}

module Perle.Mod (Mod, modInj) where

import Data.Proxy
import Numeric.Natural
import GHC.TypeNats ( KnownNat, Nat, natVal )

import Perle.Group

newtype Mod (n :: Nat) = Mod Natural
  deriving (Eq, Ord)

modInj :: forall n a. (KnownNat n, Integral a) => a -> Mod n
modInj x = Mod $ fromIntegral x `mod` natVal (Proxy @n)

modMax :: (KnownNat n) => Proxy (Mod n) -> Mod n
modMax (Proxy :: Proxy (Mod n)) =
  Mod $ natVal (Proxy @n)

modToEnum :: (KnownNat n) => Proxy (Mod n) -> Int -> Mod n
modToEnum (Proxy :: Proxy (Mod n)) n =
  Mod $ fromIntegral n `mod` natVal (Proxy @n)

instance (KnownNat n) => Bounded (Mod n) where
  minBound = Mod 0
  maxBound = modMax Proxy

instance (KnownNat n) => Enum (Mod n) where
  toEnum n = modToEnum Proxy n
  fromEnum (Mod n) = fromIntegral n

instance (KnownNat n) => Semigroup (Mod n) where
  (Mod x :: Mod n) <> (Mod y) = Mod $ x + y `mod` natVal (Proxy @n)

instance (KnownNat n) => Monoid (Mod n) where
  mempty = Mod 0

instance (KnownNat n) => Group (Mod n) where
  inv (Mod x :: Mod n) = Mod $ natVal (Proxy @n) - x
