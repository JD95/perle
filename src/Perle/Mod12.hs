module Perle.Mod12 where

import Data.Word

add12 :: Word8 -> Word8 -> Word8
add12 x y = (x + y) `mod` 12

sub12 :: Word8 -> Word8 -> Word8
sub12 x y = (12 + x - y) `mod` 12

invert12 :: Word8 -> Word8
invert12 x = (12 - x) `mod` 12
