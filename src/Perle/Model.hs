{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}

module Perle.Model
  ()
where

import System.MIDI
import System.MIDI.Utility

import GHC.Word
import Perle.Group
import Perle.Interval
import Data.Kind
import Perle.Mod
import Data.Sequence
import Data.Foldable
import Numeric.Natural

newtype PitchClass
  = PitchClass Mod

newtype Interval = Interval Word8
newtype Octave = Octave Word8

newtype Pitch
  = Pitch (Natural -> (PitchClass, Octave))

data Note
  = Note
  { pitch :: PitchClass
  , octave :: Word8
  }

class Model chord note | chord -> note, note -> chord where
  interval :: note -> note -> Interval
  intervalUp :: Interval -> note -> note
  intervalDown :: Interval -> note -> note
  transpNote :: Int -> note -> note
  --
  components :: chord -> [note]
  rotate :: Int -> chord -> chord
  transpChord :: Int -> chord -> chord

-- newtype Interval
--   = Interval Mod
--
-- interval :: Pitch -> Pitch -> Interval
-- interval (Pitch a) (Pitch b) =
--   Interval $ modSub a b
--
-- intervalUp :: Interval -> Pitch -> Pitch
-- intervalUp (Interval f) (Pitch x) =
--   Pitch $ x <> f
--
-- intervalDown :: Interval -> Pitch -> Pitch
-- intervalDown (Interval f) (Pitch x) =
--   Pitch $ x <> inv f
--
-- newtype Chord
--   = Chord { notes :: Seq Pitch }
--
-- shiftL :: Chord -> Chord
-- shiftL (Chord Empty) = Chord Empty
-- shiftL (Chord (x :<| xs)) = Chord (xs |> x)
--
-- shiftR :: Chord -> Chord
-- shiftR (Chord Empty) = Chord Empty
-- shiftR (Chord (xs :|> x)) = Chord (x <| xs)
--
-- inversion :: Int -> Chord -> Chord
-- inversion x c = iterate shiftR c !! x
--
-- components :: Chord -> [Pitch]
-- components (Chord xs) = toList xs
--
-- transpChord :: Int -> Chord -> Chord
-- transpChord t (Chord xs) = Chord $ transp t <$> xs
