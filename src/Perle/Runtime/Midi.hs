{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Perle.Runtime.Midi where

import Data.Kind
import Data.Foldable
import Control.Concurrent
import Control.Monad.State
import GHC.Word
import System.MIDI
import System.MIDI.Utility
import Control.Exception
import Control.Monad.Logic
import Control.Applicative
import Data.IORef

import Perle.AST.Expr
import Perle.Lattice.Diet
import Perle.Logic
import Perle.Lattice
import Perle.Runtime.Prop
import qualified Perle.Runtime.Prop as Prop
import Perle.Runtime.Ref
import Perle.Lattice.Logical

data Expr a
  = Div [Expr a]
  | Note a
  | Rest
  deriving (Show, Eq)

data MidiSt = MidiSt {connection :: Connection, outputChannel :: Int}

newtype Runtime a = Runtime { runtime ::  LogicT IO a }
  deriving newtype (Functor, Applicative, Alternative, Monad, MonadIO)

instance MonadRef Runtime where
  type Ref Runtime = IORef
  newRef x = liftIO $ newIORef x
  writeRef c x = liftIO $ writeIORef c x
  readRef c = liftIO $ readIORef c

instance Logical PitchSet where
  type Realized PitchSet = Word8
  assert x = PitchSet $ singleton x x
  realize (PitchSet xs) = enumerate xs

branch :: Alternative m => [a] -> m a
branch = foldr (\x xs -> pure x <|> xs) empty

run :: (MonadRef m, Alternative m) => Expr (Cell m (Ref m) PitchSet) -> m (Expr Int)
run (Div xs) =
  Div <$> traverse run xs
run (Note set) = do
  xs <- value set
  Note . fromIntegral <$> branch (realize xs)
run Rest = pure Rest

(&) = flip ($)

infixl 0 &

data PitchCell m r
  = PitchCell
  { pitchRaw :: Cell m r Pitch
  , pitchKey :: Cell m r Pitch
  , pitchFinal :: Cell m r PitchSet
  }

newPitch
  :: (Alternative m, MonadRef m) =>
  Cell m (Ref m) (Maybe (PitchClass, Accidental)) ->
  m (PitchCell m (Ref m))
newPitch keyRef = do
  xRaw <- cell bottom
  xInKey <- cell bottom
  push [Watched keyRef, Watched xRaw] xInKey $ do
    value keyRef >>= \case
      Just (p, acc) -> do
        Pitch this _ <- value xRaw
        pure $ Pitch this (key p acc <$> this)
      Nothing -> pure bottom
  xPitch <- peaceful xInKey xRaw
  xFinal <- apply $ pitch xPitch
  pure $ PitchCell xRaw xInKey xFinal

example :: IO ()
example = do
  exprs <- observeAllT $ runtime $ do
    key <- cell (Just (C, Nat))
    x <- newPitch key
    y <- newPitch key

    inform (pitchRaw x) $ Pitch (Just C) Nothing
    inform (pitchFinal x) $ range (Just 70) Nothing

    inform (pitchRaw y) $ Pitch (Just D) Nothing
    inform (pitchFinal y) $ range (Just 70) Nothing

    run $ Div [Note (pitchFinal x), Note (pitchFinal y)]
  play $ head exprs

play :: Expr Int -> IO ()
play expr = do
  dst <- selectOutputDevice "please select an output device" Nothing
  conn <- openDestination dst
  let st = MidiSt conn 1
  putStrLn "connected."
  catch (evalStateT (go expr) st) (\(SomeException _) -> pure ())
  close conn

  where

  noteOp :: (Int -> Int -> MidiMessage') -> (Int -> StateT MidiSt IO ())
  noteOp f pitchValue = do
    conn <- gets connection
    out <- gets outputChannel
    let velocity = 127
    liftIO $ send conn $ MidiMessage out (f pitchValue velocity)

  noteOn = noteOp NoteOn
  noteOff = noteOp NoteOff

  go :: Expr Int -> StateT MidiSt IO ()
  go Rest = waitSec 1.0
  go (Note p) = do
    noteOn p
    waitSec 0.25
    noteOff p
  go (Div xs) = traverse_ go xs

waitSec :: Double -> StateT MidiSt IO ()
waitSec = liftIO . threadDelay . round . (* 1000000.0)
